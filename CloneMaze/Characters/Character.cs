using Godot;
using System;
using GameManagement;

namespace Characters
{
    public class Character : Area2D
    {
        [Export]
        protected float tweenMoveSpeed = 0.5f;
        [Export]
        protected Tween.TransitionType transitionType;
        [Export]
        protected Tween.EaseType easeType;
        [Export]
        protected NodePath moveTweenPath;


        public int thisKey;
        public Vector2 direction;

        protected Tween moveTween;
        public Vector2 startPosition;
        public Vector2 previousPosition;
        protected float tileSize = 32;

        [Signal]
        public delegate void TweenDone();

        public override void _Ready()
        {
            moveTween = GetNode<Tween>(moveTweenPath) as Tween;
            if (moveTween == null) { GD.PushWarning("Character/MoveTween is NULL!!"); }
        }

        public void MoveTween(Vector2 dir)
        {
            Vector2 moveDir = new Vector2(dir * tileSize);
            moveDir = moveDir + Position;
            moveTween.InterpolateProperty(this, "position", Position, moveDir, tweenMoveSpeed, transitionType, easeType);
            moveTween.Start();
            // GD.Print("move");
        }

        public void MoveTween(Vector2 startPos, Vector2 targetPosition)
        {
            moveTween.InterpolateProperty(this, "position", startPos, targetPosition, tweenMoveSpeed, transitionType, easeType);
            moveTween.Start();
        }

        public virtual void Move()
        {

        }

        public virtual void Move(Vector2 destination)
        {
            
        }

        public void _on_MoveTween_tween_all_completed()
        {
            // GD.Print("Tween done");
            EmitSignal(nameof(TweenDone));
        }


        public virtual void MoveToStartPosition()
        {
            
        }
    }
}


