using Godot;
using System;

public class DirArrow : Sprite
{
    [Export]
    Texture normalSprite, chooseSprite;
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
       

       
    }

    public void ChangeSprite(bool choose)
    {
        if(choose) {Texture = chooseSprite;}
        else Texture = normalSprite;
    }
}
