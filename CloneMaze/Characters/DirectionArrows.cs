using Godot;
using System;

public class DirectionArrows : Node2D
{
    [Export]
    NodePath up, down, left, right;
    [Export]
    NodePath playerRays, inputController;

    PlayerRays m_playerRays;
    InputController m_inputController;

    DirArrow m_up, m_down, m_left, m_right;

    public override void _Ready()
    {
        m_up = GetNode<DirArrow>(up);
        m_down = GetNode<DirArrow>(down);
        m_left = GetNode<DirArrow>(left);
        m_right = GetNode<DirArrow>(right);

        m_playerRays = GetNode<PlayerRays>(playerRays);
        m_inputController = GetNode<InputController>(inputController);
    }


    public override void _Process(float delta)
    {
        ShowOrHideArrows();

        ChangeArrowSprite();
    }

    private void ChangeArrowSprite()
    {
        if (m_inputController.InputDir == Vector2.Up) { m_up.ChangeSprite(true); }
        else m_up.ChangeSprite(false);

        if (m_inputController.InputDir == Vector2.Down) { m_down.ChangeSprite(true); }
        else m_down.ChangeSprite(false);

        if (m_inputController.InputDir == Vector2.Left) { m_left.ChangeSprite(true); }
        else m_left.ChangeSprite(false);

        if (m_inputController.InputDir == Vector2.Right) { m_right.ChangeSprite(true); }
        else m_right.ChangeSprite(false);

        if(m_inputController.InputDir == Vector2.Zero)
        {
            m_up.ChangeSprite(false);
            m_down.ChangeSprite(false);
            m_left.ChangeSprite(false);
            m_right.ChangeSprite(false);
        }
    }

    private void ShowOrHideArrows()
    {
        if (m_playerRays.upBlocked) { m_up.Visible = false; }
        else m_up.Visible = true;

        if (m_playerRays.downBlocked) { m_down.Visible = false; }
        else m_down.Visible = true;

        if (m_playerRays.leftBlocked) { m_left.Visible = false; }
        else m_left.Visible = true;

        if (m_playerRays.rightBlocked) { m_right.Visible = false; }
        else m_right.Visible = true;
    }
}
