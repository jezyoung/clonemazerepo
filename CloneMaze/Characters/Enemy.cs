using Godot;
using System;
using GameManagement;

namespace Characters
{
    public class Enemy : Character
    {
        float m_startOffset = 0;
        float m_targetOffset;

        [Export] private float signalStartDelay;

        EnemyPath m_enemyPath;

        [Export] private NodePath m_signalPath;
        private AnimatedSprite m_enemySignalAnim;

        public override void _Ready()
        {
            AddToGroup("Enemy");
            base._Ready();
            TurnBasedManager.AddEnemy(this);
            m_enemySignalAnim = GetNode<AnimatedSprite>(m_signalPath);
            m_enemySignalAnim.SetDeferred("visible", false);
            m_targetOffset = tileSize;
            m_enemyPath = GetParent() as EnemyPath;
            startPosition = Position;
        }


        public override void Move()
        {
            MoveRoutine();
        }

        private async void MoveRoutine()
        {
            MoveTween(m_enemyPath.GetCurrentPosition(), m_enemyPath.GetNextPosition());
            await ToSignal(this, "TweenDone");
            m_startOffset = m_targetOffset;
            m_targetOffset = m_targetOffset + tileSize;
        }
        
        // Enemy hit by player
        public void _on_Enemy_area_entered(Area2D area)
        {
            if (TurnStateMachine.Instance.currentTurnState == TurnStateMachine.Instance.states["MovePlayerState"])
            {
                
                EnemyDead();
            }
        }

        private void EnemyDead()
        {
            ++GameManager.RadarCredit;
            SetDeferred("monitorable", false);
            SetDeferred("monitoring", false);
            SetDeferred("visible", false);

            
        }

        private void EnemyAlive()
        {
            SetDeferred("monitorable", true);
            SetDeferred("monitoring", true);
            SetDeferred("visible", true);
        }

        public override void MoveToStartPosition()
        {
            MoveTween(m_enemyPath.GetCurrentPosition(), startPosition);
            EnemyAlive();
        }

        public async void ShowSignal()
        {    
            Timer startDelay = new Timer();
            startDelay.OneShot = true;
            startDelay.WaitTime = signalStartDelay;
            AddChild(startDelay);
            startDelay.Start();
            await ToSignal(startDelay, "timeout");
            startDelay.QueueFree();
            
            Timer timer = new Timer();
            timer.OneShot = true;
            timer.WaitTime = 5;
            AddChild(timer);
            m_enemySignalAnim.SetDeferred("visible", true);
            timer.Start();
            await ToSignal(timer, "timeout");
            timer.QueueFree();
            m_enemySignalAnim.SetDeferred("visible", false);
        }
    }

}
