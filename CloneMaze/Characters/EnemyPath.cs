using Godot;
using System;
using Godot.Collections;

public class EnemyPath : Path2D
{
    int m_pathLength;
    int m_pathPos = 0;
    bool m_returning = false;

    public override void _Ready()
    {
        m_pathLength = Curve.GetPointCount() - 1;
    }

    public Vector2 GetCurrentPosition()
    {
        int currentPathPos = m_pathPos;

        if (!m_returning)
        {
            if (m_pathPos < m_pathLength) { ++m_pathPos; }
            if (m_pathPos == m_pathLength) { m_returning = true; }
        }
        else
        {
            if (m_pathPos > 0) { --m_pathPos; }
            if (m_pathPos == 0) { m_returning = false; }
        }

        return Curve.GetPointPosition(currentPathPos);
    }

    public Vector2 GetNextPosition()
    {
        return Curve.GetPointPosition(m_pathPos);
    }


}
