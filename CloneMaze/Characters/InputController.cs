using Godot;
using System;
using GameManagement;

public class InputController : Node
{
    [Export]
    NodePath playerRays;

    PlayerRays m_playerRays;

    private Vector2 m_inputDir;

    public Vector2 InputDir { get => m_inputDir; }

    public override void _Ready()
    {
        m_playerRays = GetNode<PlayerRays>(playerRays) as PlayerRays;
    }

    public override void _Process(float delta)
    {
        if(!GameManager.InputEnabled) {m_inputDir = Vector2.Zero;  return;}

        if (Input.IsActionJustPressed("ui_up"))
        {
            // m_playerRays.CalculateBlockedDirections();
            if (!m_playerRays.upBlocked) { m_inputDir = Vector2.Up; }
            else m_inputDir = Vector2.Zero;
        }

        else if (Input.IsActionJustPressed("ui_down"))
        {
            // m_playerRays.CalculateBlockedDirections();
            if (!m_playerRays.downBlocked) { m_inputDir = Vector2.Down; }
            else m_inputDir = Vector2.Zero;
        }

        else if (Input.IsActionJustPressed("ui_left"))
        {
            // m_playerRays.CalculateBlockedDirections();
            if (!m_playerRays.leftBlocked) { m_inputDir = Vector2.Left; }
            else m_inputDir = Vector2.Zero;
        }

        else if (Input.IsActionJustPressed("ui_right"))
        {
            // m_playerRays.CalculateBlockedDirections();
            if (!m_playerRays.rightBlocked) { m_inputDir = Vector2.Right; }
            else m_inputDir = Vector2.Zero;
        }

        else if (Input.IsActionJustPressed("ui_cancel")) { m_inputDir = Vector2.Zero; }
    }

}

