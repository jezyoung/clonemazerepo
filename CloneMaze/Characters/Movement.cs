using Godot;
using System;

namespace Characters
{
    public class Movement : Node
    {
        [Export]
        NodePath moveTween, character;

        Tween m_moveTween;
        Area2D m_character;

        float m_tileSize = 32;
        Vector2 moveDir;

        [Export]
        float m_tweenMoveSpeed = 0.5f;


        public override void _Ready()
        {
            m_moveTween = GetNode<Tween>("MoveTween") as Tween;
            if (m_moveTween == null) { GD.PushWarning("Character/Movement: Tween is NULL!!"); }

            m_character = GetParent().GetParent().GetNode<Area2D>("Player") as Area2D;
            if (m_character == null) { GD.PushWarning("Character/Movement: Character is NULL!!"); }



        }


        public override void _Process(float delta)
        {

        }

        public void Move(Vector2 dir)
        {
            GD.Print("move");
            moveDir = new Vector2(dir * m_tileSize);
            if(dir == Vector2.Zero) return;
            moveDir = moveDir + m_character.Position;
            m_moveTween.InterpolateProperty(m_character, "position", m_character.Position, dir, m_tweenMoveSpeed, Tween.TransitionType.Linear, Tween.EaseType.In);
            m_moveTween.Start();
        }
    }
}

