using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;
using GameManagement;

namespace Characters
{
    public class Player : Character
    {
        [Export] NodePath m_inputControllerPath;
        [Export] private NodePath m_radarPath;
        private Particles2D m_radar;

        InputController m_inputController;
        PackedScene m_playerCloneResource;


        private List<Character> m_playerClones = new List<Character>();
        private int m_nextClone = 0;
        public Vector2 clonePosition;

        private int m_cloneAmount = 0;

        [Signal]
        private delegate void PlayerDead();

        [Signal]
        private delegate void UpdateCloneAmount();

        public override void _Ready()
        {
            Globals.player = this;
            base._Ready();
            m_inputController = GetNode<InputController>(m_inputControllerPath);
            m_radar = GetNode<Particles2D>(m_radarPath);
            m_playerCloneResource = (PackedScene) ResourceLoader.Load("res://Characters/PlayerClone.tscn");
            startPosition = Position;
            AddClones(this);
            
            this.Connect("PlayerDead", TurnBasedManager.Instance, "OnPlayerDead");
            
        }


        public override void Move()
        {
            previousPosition = Position;
            direction = m_inputController.InputDir;
            // GD.Print("player moved");
            
            MoveTween(direction);

            if (direction == Vector2.Zero) return;
            MoveClones();
        }

        public override void _Process(float delta)
        {
        }

        public void CreateClone()
        {
            Character clone = (PlayerClone) m_playerCloneResource.Instance();
            AddClones(clone);
            CallDeferred("add_child", clone);
            clone.SetAsToplevel(true);
            Vector2 clonePos = new Vector2(m_playerClones[m_nextClone].Position.x,
                m_playerClones[m_nextClone].Position.y + tileSize);
            clone.Position = clonePos; // change
            clone.startPosition = clone.Position;
            ++m_nextClone;
            // clone.player = this;
            ++m_cloneAmount;
            EmitSignal(nameof(UpdateCloneAmount), m_cloneAmount);
        }

        void AddClones(Character clone)
        {
            m_playerClones.Add(clone);
            // ++m_cloneKey;
        }

        private async void MoveClones()
        {
            await ToSignal(this, "TweenDone");

            for (int i = 1; i < m_playerClones.Count; i++)
            {
                m_playerClones[i].Move(m_playerClones[i - 1].previousPosition);
            }
        }

        public void EmitPlayerDead()
        {
            EmitSignal(nameof(PlayerDead));
        }

        // Player hit by enemy: player dead
        public void OnPlayerAreaEntered(Area2D area)
        {
            if (area.IsInGroup("Enemy") || area.IsInGroup("Clone"))
            {
                if (TurnStateMachine.Instance.currentTurnState == TurnStateMachine.Instance.states["MoveEnemiesState"])
                {
                    EmitPlayerDead();
                }
            }

            if (area.Name == "Goal")
            {
                GameManager.playerFinished = true;
            }
        }

        public override void MoveToStartPosition()
        {
            MoveTween(Position, startPosition);
            MoveClonesToStartPosition();
        }

        private async void MoveClonesToStartPosition()
        {
            await ToSignal(this, "TweenDone");
            for (int i = 1; i < m_playerClones.Count; i++)
            {
                m_playerClones[i].Move(m_playerClones[i].startPosition);
            }
        }

        public void UseRadar()
        {
            m_radar.SetDeferred("emitting", true);
        }
    }
}