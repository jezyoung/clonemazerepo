using Godot;
using System;

namespace Characters
{
    public class PlayerClone : Character
    {

       
        
        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            AddToGroup("Clone");
        }

         // Called every frame. 'delta' is the elapsed time since the previous frame.
         public override void _Process(float delta)
         {
             
         }

         public override void Move(Vector2 destination)
         {
             previousPosition = Position;
             Position = destination;
             // make sure move happens after 
         }

         public void _on_PlayerClone_area_entered(Area2D area)
         {
             GD.Print(area.Name + " entered");
             Globals.player.EmitPlayerDead();
         }
    }
}


