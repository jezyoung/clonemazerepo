using Godot;
using System;
using Godot.Collections;

public class PlayerRays : Node2D
{
    [Export]
    NodePath rayUp, rayDown, rayLeft, rayRight;

    RayCast2D m_rayUp, m_rayDown, m_rayLeft, m_rayRight;

    public bool upBlocked = false;
    public bool downBlocked = false;
    public bool leftBlocked = false;
    public bool rightBlocked = false;

    public override void _Ready()
    {
        m_rayUp = GetNode<RayCast2D>(rayUp) as RayCast2D;
        m_rayDown = GetNode<RayCast2D>(rayDown) as RayCast2D;
        m_rayLeft = GetNode<RayCast2D>(rayLeft) as RayCast2D;
        m_rayRight = GetNode<RayCast2D>(rayRight) as RayCast2D;

        
    }

    public override void _Process(float delta)
    {
        CalculateBlockedDirections();
    }

    public void CalculateBlockedDirections()
    {
        if (m_rayUp.GetColliderShape() != 0)
        {
            upBlocked = true;
        }
        else upBlocked = false;

        if (m_rayDown.GetColliderShape() != 0)
        {
            downBlocked = true;
        }
        else downBlocked = false;

        if (m_rayLeft.GetColliderShape() != 0)
        {
            leftBlocked = true;
        }
        else leftBlocked = false;

        if (m_rayRight.GetColliderShape() != 0)
        {
            rightBlocked = true;
        }
        else rightBlocked = false;
    }
}

