using Godot;
using System;
using Godot.Collections;

namespace FSM
{
    public class StateMachine : Node
    {
        State m_state = null;
        public State state { get { return m_state; } set { SetState(value); } }
        public State previousState = null;

        public Dictionary<string, State> states = new Dictionary<string, State>();

        public override void _PhysicsProcess(float delta)
        {
            if (m_state != null)
            {
                StateLogic(delta);
                State transition = GetTransition(delta);

                if (transition != null)
                {
                    SetState(transition);
                }
            }
        }

        public virtual void StateLogic(float delta)
        {

        }

        public virtual State GetTransition(float delta)
        {
            return null;
        }

        public virtual void EnterState(State newState, State oldState)
        {

        }

        public virtual void ExitState(State oldState, State newState)
        {

        }

        public void SetState(State newState)
        {
            previousState = m_state;
            m_state = newState;

            if (previousState != null) { ExitState(previousState, newState); }

            if (newState != null) { EnterState(newState, previousState); }
        }

        public void AddState(string stateName)
        {
            State stateToAdd = new State();
            states.Add(stateName, stateToAdd);
            stateToAdd.stateName = stateName;
        }
    }

}
