using Godot;
using System;
using Characters;

namespace GameManagement
{
    public class GameManager : Node2D
    {
        private static GameManager m_instance;
        public static GameManager Instance { get => m_instance; }
        static bool m_inputEnabled = true;
        public static bool InputEnabled { get => m_inputEnabled; set => m_inputEnabled = value; }
        
        // move to globals
        private static int m_radarCredit;
        public static int RadarCredit {get => m_radarCredit; set => m_radarCredit = value; }

        public static bool playerFinished = false;
        public static bool startGame = false;
        public static bool gamePaused = false;

        private PackedScene m_gameResource;
        private PackedScene m_startScreenResource;
        private Node2D m_startScreen;
        private Node2D m_game;

        [Signal]
        private delegate void UpdateRadarAmount();
        
        public override void _Ready()
        {
            GD.Print("ping");
            m_instance = this;
            m_radarCredit = 1;
            
            m_gameResource = (PackedScene) ResourceLoader.Load("res://Game.tscn");
            m_startScreenResource = (PackedScene) ResourceLoader.Load("res://StartScreen.tscn");
            LoadStartScreen();
        }

        private void LoadStartScreen()
        {
            m_startScreen = (Node2D) m_startScreenResource.Instance();
            GetParent().GetNode<Node2D>("Main").AddChild(m_startScreen);
            m_startScreen.SetAsToplevel(true);
        }

        public override void _Process(float delta)
        {
            EmitSignal(nameof(UpdateRadarAmount), m_radarCredit);

            UseRadar();
        }

        public void StartGame()
        {
            startGame = true;
            // Node2D startScreen = GetParent().GetNode<Node2D>("Main").GetNode<Node2D>("StartScreen");
            m_startScreen.QueueFree();
            
            LoadGame();
            // load level 1
        }

        private void LoadGame()
        {
            m_game = (Node2D) m_gameResource.Instance();
            GetParent().GetNode<Node2D>("Main").AddChild(m_game);
            m_game.SetAsToplevel(true);
        }

        public void PauseGame(bool paused)
        {
            
            gamePaused = paused;
            
        }

        public void RestartGame()
        {
            m_game.QueueFree();
            startGame = false;
            LoadStartScreen();
        }

        public void QuitApplication()
        {
            GetTree().Quit();
        }

        // Move to player script
        private static void UseRadar()
        {
            if (Input.IsActionJustPressed("ui_radar"))
            {
                if (m_radarCredit > 0)
                {
                    Globals.player.UseRadar();

                    foreach (Enemy enemy in TurnBasedManager.enemies)
                    {
                        enemy.ShowSignal();
                    }

                    --m_radarCredit;
                }
            }
        }
        
        
    }
}




