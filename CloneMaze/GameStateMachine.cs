using Godot;
using System;
using FSM;
using GameManagement;

public class GameStateMachine : StateMachine
{
    private static GameStateMachine m_instance;
    public static GameStateMachine Instance { get => m_instance; }

    public State currentGameState;

    //states
    string startMenuState = "StartMenuState";
    private string pauseMenuState = "PauseMenuState";
    string chooseMoveState = "ChooseMoveState";
    string processTurnState = "ProcessTurnState";
    string playerFinishedState = "PlayerFinishedState";

    public override void _Ready()
    {
        m_instance = this;
        AddState(startMenuState);
        AddState(pauseMenuState);
        AddState(chooseMoveState);
        AddState(processTurnState);
        AddState(playerFinishedState);

        CallDeferred("SetState", states[startMenuState]);
    }

    public override void StateLogic(float delta)
    {
        // turn based management
    }

    public override State GetTransition(float delta)
    {
        if(state == states[startMenuState])
        {
            if (GameManager.startGame)
            {
                return states[chooseMoveState];
            }
        }

        else if (state == states[pauseMenuState])
        {
            if (!GameManager.gamePaused)
            {
                return previousState;
            }
        }

        else if (state == states[chooseMoveState])
        {
            if (GameManager.gamePaused)
            {
                return states[pauseMenuState];
            }
            if (Input.IsActionJustPressed("ui_accept")) return states[processTurnState];
        }

        else if (state == states[processTurnState])
        {
            if (GameManager.gamePaused)
            {
                return states[pauseMenuState];
            }
            if (GameManager.playerFinished)
            {
                return states[playerFinishedState];
            }
            if (TurnStateMachine.Instance.turnFinsished)
            {
                return states[chooseMoveState];
            }
            // if player position == goalPosition return 'playerFinishedState'
            // if turn state process is finished return chooseMoveState
        }

        else if (state == states[playerFinishedState])
        {

        }

        return null;
    }

    public override void EnterState(State newState, State oldState)
    {
        if (newState == states[startMenuState])
        {
            currentGameState = newState;
            GameManager.InputEnabled = false;
        }
        
        else if (newState == states[pauseMenuState])
        {
            GD.Print("paused");
            currentGameState = newState;
            previousState = oldState;
            GameManager.InputEnabled = false;
        }

        else if (newState == states[chooseMoveState])
        {
            currentGameState = newState;
            GameManager.InputEnabled = true;
        }

        else if (newState == states[processTurnState])
        {
            currentGameState = newState;
            GameManager.InputEnabled = false;
            // start turn routine
        }

        else if (newState == states[playerFinishedState])
        {
            // GD.Print("Player Finished");
            currentGameState = newState;
            GameManager.InputEnabled = false;
        }
    }

    public override void ExitState(State oldState, State newState)
    {

    }
}
