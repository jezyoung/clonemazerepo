using Godot;
using System;
using GameManagement;

public class StartScreen : Node2D
{

    [Signal]
    private delegate void StartGame();

    public override void _Ready()
    {
        this.Connect(nameof(StartGame), GameManager.Instance, "StartGame");
    }

    public void _on_StartButton_button_down()
    {
        GD.Print("button pressed");
        EmitSignal(nameof(StartGame));
    }
}
