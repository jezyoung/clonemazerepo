using Godot;
using System;
using System.Collections.Generic;
using Characters;

namespace GameManagement
{
    public class TurnBasedManager : Node2D
    {
        private static TurnBasedManager m_instance;
        public static TurnBasedManager Instance
        {
            get => m_instance;
        }
        
        public static List<Enemy> enemies = new List<Enemy>();

        private int m_turnAmount = 0;
        // public Dictionary<int, Enemy> enemies;
        // private int key = 0;
        
        // REMOVE?
        [Signal]
        public delegate void CharacterBackAtStartPos();

        [Signal]
        private delegate void UpdateTurnAmount();

        public bool playerMoveFinished = false;
        public bool enemiesMoveFinished = false;
        public bool playerDead = false;
        public bool allBackAtStartPos = false;
        public bool cloneCreated = false;

        public override void _Ready()
        {
            m_instance = this;
            // enemies = new Dictionary<int, Enemy>();
        }

        public static void AddEnemy(Enemy enemy)
        {
            enemies.Add(enemy);
            // enemies.Add(key, enemy);
            // int x = key;
            // key++;
            // return x;
        }

        public void StartNewTurn()
        {
            ++m_turnAmount;
            EmitSignal(nameof(UpdateTurnAmount), m_turnAmount);
            playerDead = false;
            playerMoveFinished = false;
            enemiesMoveFinished = false;
            allBackAtStartPos = false;
            cloneCreated = false;
        }

        public void MovePlayer()
        {
            AwaitPlayerMoveFinished();
        }

        private async void AwaitPlayerMoveFinished()
        {
            playerMoveFinished = false;
            Globals.player.Move();
            await ToSignal(Globals.player, "TweenDone");
            playerMoveFinished = true;
        }

        public void MoveEnemies()
        {
            AwaitEnemiesMoveFinished();
        }

        private async void AwaitEnemiesMoveFinished()
        {
            enemiesMoveFinished = false;
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].Move();
                await ToSignal(enemies[i], "TweenDone");
            }

            enemiesMoveFinished = true;
        }

        public void OnPlayerDead()
        {
            playerDead = true;
        }

        public void MoveAllToStartPos()
        {
            AwaitAllMovedToStartPos();
        }

        private async void AwaitAllMovedToStartPos()
        {
            allBackAtStartPos = false;
            Globals.player.MoveToStartPosition();
            await ToSignal(Globals.player, "TweenDone");

            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].MoveToStartPosition();
                await ToSignal(enemies[i], "TweenDone");
            }

            allBackAtStartPos = true;
        }

        public void CreateClone()
        {
            AwaitCreateClone();
        }

        private async void AwaitCreateClone()
        {
            cloneCreated = false;
            Timer timer = new Timer();
            timer.OneShot = true;
            timer.WaitTime = 0.1f;
            AddChild(timer);

            Globals.player.CreateClone();

            timer.Start();
            await ToSignal(timer, "timeout");
            timer.QueueFree();
            cloneCreated = true;
        }
    }
}