using Godot;
using System;
using FSM;
using GameManagement;

public class TurnStateMachine : StateMachine
{
    private static TurnStateMachine m_instance;
    public static TurnStateMachine Instance { get => m_instance;}

    String idleState = "IdleState";
    String movePlayerState = "MovePlayerState";
    String moveEnemiesState = "MoveEnemiesState";
    String moveToStartPosState = "MoveToStartPosState";
    String clonePlayerState = "ClonePlayerState";

    public bool turnFinsished = true;
    public State currentTurnState;

    public override void _Ready()
    {
        m_instance = this;

        AddState(idleState);
        AddState(movePlayerState);
        AddState(moveEnemiesState);
        AddState(moveToStartPosState);
        AddState(clonePlayerState);

        CallDeferred("SetState", states[idleState]);
    }


    public override void StateLogic(float delta)
    {
        // turn based management
    }

    public override State GetTransition(float delta)
    {
        if (state == states[idleState])
        {
            
            if(GameStateMachine.Instance.currentGameState == GameStateMachine.Instance.states["ProcessTurnState"])
            {
                return states[movePlayerState];
            }
        }

        else if (state == states[movePlayerState])
        {
            // if player move finished return states[moveEnemiesState]
            if (TurnBasedManager.Instance.playerMoveFinished)
            {
                return states[moveEnemiesState];
            }
            
        }

        else if (state == states[moveEnemiesState])
        {
            if (TurnBasedManager.Instance.playerDead)
            {
                return states[moveToStartPosState];
            }
            if (TurnBasedManager.Instance.enemiesMoveFinished)
            {
                return states[idleState];
            }
            // if enemy killed return states[idleState]
            // if player killed return states[moveToStartPosState]
        }

        else if (state == states[moveToStartPosState])
        {
            if (TurnBasedManager.Instance.allBackAtStartPos)
            {
                return states[clonePlayerState];
            }
        }

        else if (state == states[clonePlayerState])
        {
            if (TurnBasedManager.Instance.cloneCreated)
            {
                return states[idleState];
            }
        }

        return null;
    }

    public override void EnterState(State newState, State oldState)
    {
        if (newState == states[idleState])
        {
            
            currentTurnState = newState;
            turnFinsished = true;
        }

        else if (newState == states[movePlayerState])
        {
            currentTurnState = newState;
            // GD.Print("entered moveplayerstate");
            TurnBasedManager.Instance.MovePlayer();
        }

        else if (newState == states[moveEnemiesState])
        {
            currentTurnState = newState;
            // GD.Print("entered moveEnemiesState");
            TurnBasedManager.Instance.MoveEnemies();
         
        }

        else if (newState == states[moveToStartPosState])
        {
            currentTurnState = newState;
            // GD.Print("entered moveToStartPosState");
            TurnBasedManager.Instance.MoveAllToStartPos();
        }

        else if (newState == states[clonePlayerState])
        {
            currentTurnState = newState;
            // GD.Print(("entered clonePlayerState"));
            TurnBasedManager.Instance.CreateClone();
        }
    }

    public override void ExitState(State oldState, State newState)
    {
        if (oldState == states[idleState])
        {
            turnFinsished = false;
            TurnBasedManager.Instance.StartNewTurn();
        }
    }
}
