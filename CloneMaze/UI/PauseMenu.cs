using Godot;
using System;
using GameManagement;

public class PauseMenu : Node2D
{
    private Popup m_popup;

    [Signal]
    private delegate void PauseMenuOpen();
    [Signal]
    private delegate void PauseMenuClosed();
    [Signal]
    private delegate void Resume();
    [Signal]
    private delegate void Restart();
    [Signal]
    private delegate void Quit();
    

    public override void _Ready()
    {
        m_popup = GetChild(0).GetNode<Popup>("Popup");
        this.Connect(nameof(PauseMenuOpen), GameManager.Instance, "PauseGame");
        this.Connect(nameof(PauseMenuClosed), GameManager.Instance, "PauseGame");
        this.Connect(nameof(Resume), GameManager.Instance, "PauseGame");
        this.Connect(nameof(Restart), GameManager.Instance, "RestartGame");
        this.Connect(nameof(Quit), GameManager.Instance, "QuitApplication");
    }

    public override void _Process(float delta)
    {
        if (Input.IsActionJustPressed("ui_pause"))
        {
           m_popup.PopupCentered();
        }
    }

    public void _on_Popup_about_to_show()
    {
        GD.Print("popup show");
        EmitSignal(nameof(PauseMenuOpen), true);
    }

    public void _on_Popup_popup_hide()
    {
        GD.Print("popup hide");
        EmitSignal(nameof(PauseMenuClosed), false);
    }

    public void _on_ResumeButton_button_down()
    {
        m_popup.Hide();
        GD.Print("resume");
        EmitSignal(nameof(Resume), false);
    }

    public void _on_RestartButton_button_down()
    {
        GD.Print("restart");
        EmitSignal(nameof(Restart));
    }

    public void _on_QuitButton_button_down()
    {
        GD.Print("quit");
        EmitSignal(nameof(Quit));
    }
}