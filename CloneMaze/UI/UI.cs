using Godot;
using System;
using GameManagement;
using Godot.Collections;
using Characters;


public class UI : CanvasLayer
{
    [Export] private NodePath turnPath, radarPath, clonePath;

    private Label turnValueLabel, radarValueLabel, cloneValueLabel;

    public override void _Ready()
    {
        turnValueLabel = GetNode<Label>(turnPath);
        radarValueLabel = GetNode<Label>(radarPath);
        cloneValueLabel = GetNode<Label>(clonePath);

        TurnBasedManager.Instance.Connect("UpdateTurnAmount",this, nameof(UpdateTurnValue));
        GameManager.Instance.Connect("UpdateRadarAmount", this, nameof(UpdateRadarValue));
        Globals.player.Connect("UpdateCloneAmount", this, nameof(UpdateCloneValue));
        
    }

    private void UpdateTurnValue(int turnValue)
    {
        turnValueLabel.Text = turnValue.ToString();
    }
    
    private void UpdateRadarValue(int radarValue)
    {
        radarValueLabel.Text = radarValue.ToString();
    }
    
    private void UpdateCloneValue(int cloneValue)
    {
        cloneValueLabel.Text = cloneValue.ToString();
    }
    
}
